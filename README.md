# Setup

1. Install NodeJS 6.6.0
2. Install and run MongoDB Server
 * [Windows, Linux, Mac](https://docs.mongodb.com/manual/installation/)
 * [Cloud9](https://community.c9.io/t/setting-up-mongodb/1717)
3. Run `npm install` in ./server and ./client
4. Start server `node server.js`

Demo video of application: https://drive.google.com/file/d/0B9N8qjVm4R_Hem10RjNaajhnOWc/view?usp=sharing