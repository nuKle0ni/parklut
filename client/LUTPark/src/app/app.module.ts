import {NgModule} from '@angular/core';
import {IonicApp, IonicModule} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Http} from '@angular/http';
import {AuthHttp, AuthConfig} from 'angular2-jwt';
import {MyApp} from './app';
import {HomePage} from './pages/home/home';
import {ListPage} from './pages/list/list';
import {LoginPage} from './pages/login/login';
import {OfferPage} from './pages/offer/offer';
import {RentPage} from './pages/rent/rent';
import {AuthService} from './auth.service';
import {ParkingAreaService} from './parkingarea.service';
import {AboutPage} from './pages/about/about';
import {SpotsPage} from './pages/spots/spots';
import {ParkingSpotService} from './parking-spot.service';
import {BorrowEventService} from "./borrow-event.service";
import {MomentModule} from "angular2-moment";
import {Logger} from "angular2-logger/core";
import {Config} from "./config";


export function getAuthHttp(http, storage) {
  return new AuthHttp(new AuthConfig({
    headerPrefix: 'JWT',
    noJwtError: true,
    globalHeaders: [{'Accept': 'application/json'}],
    tokenGetter: (() => storage.get('token')),
  }), http);
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    OfferPage,
    RentPage,
    AboutPage,
    SpotsPage,
  ],
  imports: [
    MomentModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    LoginPage,
    OfferPage,
    RentPage,
    AboutPage,
    SpotsPage,
  ],
  providers: [{
    provide: AuthHttp,
    useFactory: getAuthHttp,
    deps: [Http, Storage]
  }, Logger, AuthService, Storage, ParkingAreaService, ParkingSpotService, BorrowEventService, Config
  ]

})
export class AppModule {
}
