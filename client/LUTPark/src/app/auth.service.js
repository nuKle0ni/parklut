"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var AuthService = (function () {
  function AuthService(http) {
    this.loginRoute = 'http://localhost:3000/api/auth/login';
    this.registerRoute = 'http://localhost:3000/api/auth/register';
    this.http = http;
  }

  AuthService.prototype.login = function (credentials) {
    return this.postCredentials(this.loginRoute, credentials);
  };
  AuthService.prototype.register = function (credentials) {
    return this.postCredentials(this.registerRoute, credentials);
  };
  AuthService.prototype.postCredentials = function (route, credentials) {
    var body = JSON.stringify({"email": credentials.email, "password": credentials.password});
    console.log(body);
    var headers = new http_1.Headers({'Content-Type': 'application/json'});
    var options = new http_1.RequestOptions({headers: headers});
    return this.http.post(route, body, options);
  };
  AuthService = __decorate([
    core_1.Injectable()
  ], AuthService);
  return AuthService;
}());
exports.AuthService = AuthService;
