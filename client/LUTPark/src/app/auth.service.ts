import {Injectable} from '@angular/core';
import {Platform} from 'ionic-angular';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Credentials} from './credentials';
import {Storage}from '@ionic/storage';
import {Config} from "./config";

@Injectable()
export class AuthService {
  loginRoute: string;
  registerRoute: string;

  constructor(public http: Http, public storage: Storage, public platform: Platform, config: Config) {
    this.loginRoute = config.API_URL + "/auth/login";
    this.registerRoute = config.API_URL + "/auth/register";
  }

  logOut() {
    this.storage.set('token', null);
  }

  login(credentials: Credentials, callback: Function): Observable<any> {
    return this.postCredentials(this.loginRoute, credentials).map((res: Response) => {
      console.log("login" + res.json().token);
      this.storage.set('token', res.json().token).then(() => callback());
      return res;
    });
  }

  register(credentials: Credentials, callback: Function): Observable<any> {
    return this.postCredentials(this.registerRoute, credentials).map((res: Response) => {
      console.log("register" + res.json().token);
      this.storage.set('token', res.json().token).then(() => callback());
      return res;
    });
  }

  private postCredentials(route: string, credentials: Credentials) {
    let body = JSON.stringify({'email': credentials.email, 'password': credentials.password});
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.post(route, body, options);
  }

  isLoggedIn() {
    return this.storage.get('token');
  }
}
