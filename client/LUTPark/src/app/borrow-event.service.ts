import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import 'rxjs/add/operator/map';
import {AuthHttp} from "angular2-jwt";
import {BorrowEvent} from "./borrow-event";
import {Config} from "./config";

@Injectable()
export class BorrowEventService {
  eventsRoute: string;

  constructor(private authHttp: AuthHttp, private config: Config) {
    this.eventsRoute = config.API_URL + "/borrow-events";
  }

  public getBorrowEvents(): Observable<BorrowEvent[]> {
    return this.authHttp.get(this.eventsRoute).map(res => res.json());
  }
}
