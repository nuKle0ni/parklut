import {User} from './user';
export interface BorrowEvent {
  _id: string,
  startTime: Date,
  endTime: Date,
  borrower: User
}
