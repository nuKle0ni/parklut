import {Injectable} from "@angular/core";
@Injectable()
export class Config {
  //public API_URL = "http://localhost:3000/api";
  //Production...
  //TODO get this from env variable instead
  public API_URL = "https://parklut.herokuapp.com/api";

  constructor() {
  }
}
