import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {HomePage} from '../home/home'

@Component({
  templateUrl: 'about.html'
})
export class AboutPage {
  homePage = HomePage;

  constructor(public navCtrl: NavController) {

  }
}
