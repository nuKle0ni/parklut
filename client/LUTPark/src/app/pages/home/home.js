"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
var core_1 = require('@angular/core');
var about_1 = require('../about/about');
var HomePage = (function () {
  function HomePage(platform) {
    var _this = this;
    this.aboutPage = about_1.AboutPage;
    platform.ready().then(function () {
      _this.createMap();
    });
  }

  HomePage.prototype.ionViewDidEnter = function () {
    google.maps.event.trigger(this.map, "resize");
  };
  HomePage.prototype.createMap = function () {
    var latLng = new google.maps.LatLng(61.065119, 28.092662);
    var mapOptions = {
      center: latLng,
      zoom: 16,
      mapTypeControl: false,
      streetViewControl: false,
      zoomControl: false
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    new google.maps.Marker({
      map: this.map,
      position: {lat: 61.064211, lng: 28.089606}
    });
    //TODO commented out temporarily, was giving out undefined error shortly after initializing
    //Sets bounds for the map movement
    // var strictBounds = new google.maps.LatLngBounds(
    //   new google.maps.LatLng(61.063424, 28.087842),
    //   new google.maps.LatLng(61.067172, 28.098024)
    // );
    // Listen for the dragend event
    // google.maps.event.addListener(this.map, 'dragend', function () {
    //   if (strictBounds.contains(this.map.getCenter())) return;
    //
    //   // We're out of bounds - Move the map back within the bounds
    //   var c = this.map.getCenter(),
    //     x = c.lng(),
    //     y = c.lat(),
    //     maxX = strictBounds.getNorthEast().lng(),
    //     maxY = strictBounds.getNorthEast().lat(),
    //     minX = strictBounds.getSouthWest().lng(),
    //     minY = strictBounds.getSouthWest().lat();
    //
    //   if (x < minX) x = minX;
    //   if (x > maxX) x = maxX;
    //   if (y < minY) y = minY;
    //   if (y > maxY) y = maxY;
    //
    //   this.map.setCenter(new google.maps.LatLng(y, x));
    // });
  };
  __decorate([
    core_1.ViewChild('map')
  ], HomePage.prototype, "mapElement");
  HomePage = __decorate([
    core_1.Component({
      templateUrl: 'build/pages/home/home.html'
    })
  ], HomePage);
  return HomePage;
}());
exports.HomePage = HomePage;
