import {Component, ViewChild, ElementRef} from '@angular/core';
import {Platform, NavController} from 'ionic-angular';
import {AboutPage} from '../about/about'
import {ListPage} from '../list/list';
import {LoginPage} from '../login/login';
import {SpotsPage} from '../spots/spots';
import {ParkingAreaService} from '../../parkingarea.service';
import {AuthService} from "../../auth.service";

declare var google;

@Component({
  templateUrl: 'home.html'
})
export class HomePage {
  aboutPage = AboutPage;
  spotsPage = SpotsPage;

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  constructor(private navCtrl: NavController, platform: Platform, private parkingAreaService: ParkingAreaService, private authService: AuthService) {
    platform.ready().then(() => {
      this.createMap();
      this.addParkingSpots();
    });
  }

  logOut() {
    this.authService.logOut();
    this.navCtrl.setRoot(LoginPage, {"skipLoggedInCheck": true});
  }

  ionViewDidEnter() {
    google.maps.event.trigger(this.map, 'resize');
  }


  createMap() {
    let latLng = new google.maps.LatLng(61.065119, 28.092662);

    let mapOptions = {
      center: latLng,
      zoom: 16,
      streetViewControl: false,
      fullscreenControl: false
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  }

  private addParkingSpots() {
    this.parkingAreaService.getParkingAreas().subscribe(
      areas => {
        console.log('Adding markers...', areas.length);
        areas.forEach(area => {
          let newMarker = new google.maps.Marker({
            map: this.map,
            position: {lat: area.location.lat, lng: area.location.lng}
          });
          newMarker.addListener('click', () => this.navCtrl.push(ListPage, {area: area}));
        });
      },
      error => {
        //failed, maybe user is not logged in?
        console.log(error);
      });
  }
}
