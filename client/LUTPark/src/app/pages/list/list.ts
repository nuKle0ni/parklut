import {Component} from '@angular/core';
import {NavParams, NavController} from 'ionic-angular';
import {RentPage} from '../rent/rent';
import * as moment from 'moment';
import {ParkingArea} from '../../parking-area';
import {ParkingSpot} from '../../parking-spot';
import {ParkingAreaService} from "../../parkingarea.service";

@Component({
  templateUrl: 'list.html'
})
export class ListPage {
  parkingArea: ParkingArea;
  allSpots: ParkingSpot[] = [];
  freeSpots: ParkingSpot[] = [];
  reservedSpots: ParkingSpot[] = [];

  constructor(navParams: NavParams, private navCtrl: NavController, private areaService: ParkingAreaService) {
    this.parkingArea = navParams.get('area');
    this.allSpots = this.parkingArea.parkingSpots;
  }

  ionViewDidEnter() {
    console.log("Entering ListPage");
    return this.areaService.getParkingSpotsForArea(this.parkingArea).subscribe(
      (spots) => {
        this.allSpots = spots;
        this.refreshList();
      },
      (error) => console.log(error)
    );
  }

  private refreshList() {
    this.freeSpots = [];
    this.reservedSpots = [];
    this.allSpots.forEach(spot => {
      if (this.isFreeNow(spot)) {
        this.freeSpots.push(spot);
      } else {
        this.reservedSpots.push(spot);
      }
    });
  }

  spotClicked(spot: ParkingSpot) {
    this.navCtrl.push(RentPage, {spot: spot});
  }

  /**
   * Checks if spot is free at this moment
   *
   * @param spot the spot to check
   * @returns {boolean} true if it is free
   */
  isFreeNow(spot: ParkingSpot): boolean {
    let now = moment();
    if (spot.tenant && spot.tenant.length > 0) {
      //if it has tenant but tenant has set it free today
      return this.spotHasFreeDateNow(spot) && this.spotDoesntHaveBorrowEventsNow(spot);
    }
    //no tenant, check only for borrow events
    else {
      return this.spotDoesntHaveBorrowEventsNow(spot);
    }
  }

  spotHasFreeDateNow(spot: ParkingSpot): boolean {
    if (!spot.freeDates) {
      return false;
    }
    let now = moment();
    return spot.freeDates.find(date => now.isSame(date, "day")) != null;
  }

  spotDoesntHaveBorrowEventsNow(spot: ParkingSpot): boolean {
    if (!spot.borrowEvents) {
      return true;
    }
    let now = moment();
    return spot.borrowEvents.find(be => now.isBetween(be.startTime, be.endTime)) == null;
  }
}
