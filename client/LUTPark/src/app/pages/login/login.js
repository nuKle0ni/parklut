"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
var core_1 = require('@angular/core');
var home_1 = require('../home/home');
var LoginPage = (function () {
  function LoginPage(navCtrl, toastCtrl, authService) {
    this.homePage = home_1.HomePage;
    this.credentials = {email: "", password: ""};
    this.navCtrl = navCtrl;
    this.authService = authService;
    this.toastCtrl = toastCtrl;
  }

  LoginPage.prototype.login = function () {
    var _this = this;
    this.authService.login(this.credentials).subscribe(function (res) {
      _this.navCtrl.setRoot(home_1.HomePage);
    }, function () {
      _this.toastCtrl.create({
        duration: 3000,
        message: "Invalid username or password!",
        position: 'bottom'
      }).present();
    });
  };
  LoginPage.prototype.register = function () {
    var _this = this;
    this.authService.register(this.credentials).subscribe(function () {
      _this.navCtrl.setRoot(home_1.HomePage);
    }, function (error) {
      console.log(error);
      _this.toastCtrl.create({
        duration: 3000,
        message: "Registration failed",
        position: 'bottom'
      }).present();
    });
  };
  LoginPage = __decorate([
    core_1.Component({
      templateUrl: 'build/pages/login/login.html'
    })
  ], LoginPage);
  return LoginPage;
}());
exports.LoginPage = LoginPage;
