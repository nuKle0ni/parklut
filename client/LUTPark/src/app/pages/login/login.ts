import {Component} from '@angular/core';
import {HomePage} from '../home/home';
import {AuthService} from '../../auth.service';
import {NavController, ToastController, NavParams} from "ionic-angular";
import {Credentials} from "../../credentials";

@Component({
  templateUrl: 'login.html'
})
export class LoginPage {
  homePage = HomePage;
  credentials: Credentials = {email: "", password: ""};
  authSuccessCallback = () => this.navCtrl.setRoot(HomePage);

  constructor(private navParams: NavParams, private navCtrl: NavController, private toastCtrl: ToastController, private authService: AuthService) {
    this.authService = authService;
    if (!navParams.get("skipLoggedInCheck")) {
      this.authService.isLoggedIn().then((loggedIn) => {
        if (loggedIn) {
          this.navCtrl.setRoot(this.homePage);
        }
      });
    }

  }

  login() {
    this.authService.login(this.credentials, this.authSuccessCallback).subscribe(
      console.log,
      error => {
        console.log(error);
        this.toastCtrl.create({
          duration: 3000,
          message: "Invalid username or password!",
          position: 'bottom',
        }).present();
      });
  }

  register() {
    this.authService.register(this.credentials, this.authSuccessCallback).subscribe(
      console.log
      , error => {
        console.log(error);
        this.toastCtrl.create({
          duration: 3000,
          message: "Registration failed",
          position: 'bottom',
        }).present();
      });
  }
}
