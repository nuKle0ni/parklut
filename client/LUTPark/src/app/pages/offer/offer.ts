import {Component} from '@angular/core';
import {NavParams, AlertController, ToastController} from 'ionic-angular';
import {ParkingSpot} from '../../parking-spot';
import {ParkingSpotService} from '../../parking-spot.service';
import * as moment from 'moment';

@Component({
  templateUrl: 'offer.html'
})
export class OfferPage {
  readonly DATE_FORMAT = "DD-MM-YYYY";
  parkingSpot: ParkingSpot;
  startDate: String;
  endDate: String;
  today: String;

  constructor(navParams: NavParams, private alertCtrl: AlertController, private spotService: ParkingSpotService, private toastCtrl: ToastController) {
    this.parkingSpot = navParams.get('spot');
    this.startDate = moment().toISOString();
    this.today = moment().toISOString();
  }

  deleteFreeDate(dateToDelete: Date) {
    console.log("clicked");
    this.spotService.deleteFreeDate(this.parkingSpot, dateToDelete).subscribe((spot) => {
        this.parkingSpot = spot;
        this.toastCtrl.create({
          message: "Deleted",
          duration: 3000
        }).present();
    }, (err) =>
      console.log(err));
  }

  showAlert() {
    console.log(this.startDate);
    console.log(this.endDate);
    let alert = this.alertCtrl.create({
      title: 'Confirm your offer',
      message: 'Spot: ' + this.parkingSpot.name +
      '<br>Start date: ' + moment(this.startDate).format(this.DATE_FORMAT),
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            this.spotService.setFreeDates(this.parkingSpot, this.startDate, this.endDate).subscribe(
              (spot) => {
                this.parkingSpot = spot;
                this.toastCtrl.create({
                  duration: 3000,
                  message: 'Spot set available!',
                  position: 'bottom',
                }).present();
              },
              (err) => {
                this.toastCtrl.create({
                  duration: 3000,
                  message: 'Failed... Try again!',
                  position: 'bottom',
                }).present();
              });
          }
        }
      ]
    });
    alert.present();
  }
}
