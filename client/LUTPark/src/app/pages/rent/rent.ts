import {Component} from '@angular/core';
import {NavParams, NavController, ToastController} from 'ionic-angular';
import {ParkingSpot} from "../../parking-spot";
import * as moment from "moment";
import {ParkingSpotService} from "../../parking-spot.service";

@Component({
  templateUrl: 'rent.html'
})
export class RentPage {
  readonly TIME_FORMAT = 'HH:mm';

  parkingSpot: ParkingSpot;
  minValue = 5;
  maxValue = 180;
  sliderValue = this.minValue;
  timeNow: string;
  endTime: string;


  constructor(navParams: NavParams, private toastCtrl: ToastController, private navCtrl: NavController, private spotService: ParkingSpotService) {
    this.parkingSpot = navParams.get('spot');
    this.timeNow = moment().toISOString();
    this.endTime = moment().add(this.sliderValue, 'm').toISOString();
  }

  /**
   * Called when slider changes
   */
  onChange() {
    this.timeNow = moment().toISOString();
    this.endTime = moment().add(this.sliderValue, 'm').toISOString();
  }


  confirmClicked() {
    this.onChange();
    console.log(this.timeNow + "---" + this.endTime);

    this.spotService.createBorrowEvent(this.parkingSpot, this.timeNow, this.endTime).subscribe(
      () => {
        this.toastCtrl.create({
          duration: 3000,
          message: `Rented a spot from 
          ${moment(this.timeNow).format(this.TIME_FORMAT)} 
          to
          ${moment(this.endTime).format(this.TIME_FORMAT)}`
        }).present();
        this.navCtrl.pop();
      },
      (err) => this.toastCtrl.create({duration: 3000, message: "Something went wrong :("}).present());
  }
}
