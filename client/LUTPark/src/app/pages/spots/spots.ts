import {Component} from '@angular/core';
import {NavParams, NavController} from 'ionic-angular';
import {HomePage} from "..home/home";
import {OfferPage} from "../offer/offer";
import {ParkingSpot} from "../../parking-spot";
import {ParkingSpotService} from "../../parking-spot.service";


@Component({
  templateUrl: 'spots.html'
})
export class SpotsPage {
  mySpots: ParkingSpot[];

  constructor(private navCtrl: NavController, private spotService: ParkingSpotService) {
  }

  ionViewDidEnter() {
    this.spotService.getMySpots().subscribe(spots => this.mySpots = spots);
  }

  spotClicked(spot: ParkingSpot) {
    this.navCtrl.push(OfferPage, {spot: spot});
  }
}
