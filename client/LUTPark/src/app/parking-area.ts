import {ParkingSpot} from "./parking-spot";
export interface ParkingArea {
  _id: string,
  name: string,
  location: {
    lat: number,
    lng: number
  },
  parkingSpots: ParkingSpot[]
}
