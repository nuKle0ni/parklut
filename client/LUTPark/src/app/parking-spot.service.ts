import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import {AuthHttp} from 'angular2-jwt';
import {Headers, RequestOptions} from '@angular/http';
import {ParkingSpot} from './parking-spot';
import {BorrowEvent} from './borrow-event';
import {Config} from "./config";

@Injectable()
export class ParkingSpotService {
  authHttp: AuthHttp;
  spotRoute: string;

  constructor(authHttp: AuthHttp, private config: Config) {
    this.authHttp = authHttp;
    this.spotRoute = config.API_URL + "/parking-spots/";
  }

  public getMySpots(): Observable<ParkingSpot[]> {
    return this.authHttp.get(this.spotRoute + 'my-spots').map(res => res.json());
  }

  public getBorrowEventsForSpot(spot: ParkingSpot): Observable<BorrowEvent[]> {
    return this.authHttp.get(this.spotRoute + spot._id + '/borrow-events').map(res => res.json());
  }

  public createBorrowEvent(spot: ParkingSpot, startTime: String, endTime: String): Observable<BorrowEvent> {
    let body = JSON.stringify({startTime: startTime, endTime: endTime});
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    console.log(body);
    return this.authHttp.post(this.spotRoute + spot._id + '/borrow-events', body, options).map(res => res.json());
  }

  public setFreeDates(spot: ParkingSpot, startDate: String, endDate: String): Observable<ParkingSpot> {
    let body = JSON.stringify({'startDate': startDate, 'endDate': endDate});
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    console.log(body);
    return this.authHttp.patch(this.spotRoute + spot._id + '/free-dates', body, options).map(res => res.json());
  }

  public deleteFreeDate(spot: ParkingSpot, dateToDelete: Date): Observable<ParkingSpot> {
    let body = JSON.stringify({'date': dateToDelete});
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    options.body = body;
    console.log(options);
    return this.authHttp.delete(this.spotRoute + spot._id + '/free-dates', options).map(res => res.json());
  }
}
