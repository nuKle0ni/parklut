import {BorrowEvent} from "./borrow-event";
export interface ParkingSpot {
  _id: string,
  name: string,
  tenant?: string,
  location: {lat: number, lon: number},
  freeDates: [Date],
  borrowEvents: [BorrowEvent]
}
