import {Injectable} from "@angular/core";
import {AuthHttp} from 'angular2-jwt';
import {Observable} from "rxjs";
import {ParkingArea} from "./parking-area";
import 'rxjs/add/operator/map';
import {ParkingSpot} from "./parking-spot";
import {Config} from "./config";

@Injectable()
export class ParkingAreaService {
  parkingAreaRoute: string;

  constructor(private authHttp: AuthHttp, config: Config) {
    this.parkingAreaRoute = config.API_URL + "/parking-areas/";
  }

  public getParkingAreas(): Observable<ParkingArea[]> {
    return this.authHttp.get(this.parkingAreaRoute).map(res => res.json());
  }

  public getParkingSpotsForArea(area: ParkingArea): Observable<ParkingSpot[]> {
    return this.authHttp.get(this.parkingAreaRoute + area._id).map(res => {
      let area = res.json();
      return area.parkingSpots;
    });
  }
}
