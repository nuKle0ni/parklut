import {BorrowEvent} from "./borrow-event";
export interface User {
  email: string,
  borrowEvents: BorrowEvent[]
}
