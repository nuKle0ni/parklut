const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('./models/user.js');
const config = require('./config.js');

const localStrategy = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    session: false
  },
  (email, password, done) => {
    User.findOne({email: email}, function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false, {message: 'Incorrect username.'});
      }
      user.verifyPassword(password, (err, valid) => {
        if (err) {
          return done(err);
        }
        if (!valid) {
          return done(null, false, {message: 'Incorrect password.'});
        }
        return done(null, user);
      });
    });
  });

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeader(),
  secretOrKey: config.jwtSecret
};

const jwtStrategy = new JwtStrategy(opts, function (jwt_payload, done) {
  User.findById(jwt_payload.id, function (err, user) {
    if (err) {
      return done(err, false);
    }
    if (user) {
      done(null, user);
    } else {
      done(null, false);
    }
  });
});

module.exports.localStrategy = localStrategy;
module.exports.jwtStrategy = jwtStrategy;
