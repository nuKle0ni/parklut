module.exports = {
  jwtSecret: process.env.JWTSECRET || "dev-secret",
  mongoUrl: (process.env.MONGODB_URI || "mongodb://127.0.0.1") + '/parklut'
};
