const User = require('../models/user.js');
const config = require('../config');
const jwt = require('jsonwebtoken');

const register = (req, res) => {
  const newUser = new User({
    email: req.body.email,
    password: req.body.password
  });
  newUser.save((err, user) => {
    if (err) {
      console.log(err);
      res.status(400);
      return res.json({success: false, error: err});
    }
    return res.json({success: true, user: user, token: jwt.sign({id: user._id}, config.jwtSecret)});
  });
};

const login = (req, res) => res.json({
  success: true,
  user: req.user,
  token: jwt.sign({id: req.user._id}, config.jwtSecret)
});

module.exports = {register, login};
