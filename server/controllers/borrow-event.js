const BorrowEvent = require('../models/borrow-event');

const getAll = (req, res) => {
  BorrowEvent.find().populate('spot').populate('borrower').exec((err, events)=> {
    if (err)  return console.log(err);
    return res.json(events);
  });
};

const create = (req, res) => {
  let borrowEvent = BorrowEvent(req.body);
  BorrowEvent.create(req.body, (err, event) => {
    if (err)  return console.log(err);
    return res.status(200).send(event);
  });
};

module.exports = {getAll, create};
