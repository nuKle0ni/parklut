const ParkingArea = require('../models/parking-area');

const getAll = (req, res) => {
  ParkingArea.find().populate('parkingSpots').exec((err, areas) => {
    if (err)  return console.log(err);
    res.json(areas);
  });
};

const getById = (req, res) => {
  ParkingArea.findById(req.params.id).populate('parkingSpots').deepPopulate('parkingSpots.borrowEvents').exec((err, area) => {
    if (err)  return console.log(err);
    res.json(area);
  });
};

module.exports = {getAll, getById};
