const ParkingSpot = require('../models/parking-spot');
const BorrowEvent = require('../models/borrow-event');
var moment = require('moment');

const getById = (req, res) => {
  ParkingSpot.findOne({_id: reg.params.id}).populate('tenant').exec((err, spot) => {
    if (err)  return res.status(500);
    if (!spot)  return res.status(404);
    return res.json(spot);
  });
};

const getAll = (req, res) => {
  ParkingSpot.find().populate('tenant').populate('borrowEvents').exec((err, spots) => {
    if (err) return res.status(500);
    return res.json(spots);
  });
};

const getMySpots = (req, res) => {
  console.log(req.user);
  ParkingSpot.find({tenant: req.user._id}).exec((err, spots) => {
    if (err)  return res.status(500);
    return res.json(spots);
  });
};

const addFreeDates = (req, res) => {
  ParkingSpot.findById(req.params.id, (err, spot) => {
    if (err)  return res.status(500);
    if (!spot) return res.status(404);
    var startDate = moment(req.body.startDate).toDate();
    //Dont save duplicates
    if (indexOfSameDate(spot.freeDates, startDate) === -1) {
      spot.freeDates.push(startDate);
      spot.save((err, savedSpot) => {
        if (err)  return res.status(500);
        console.log(savedSpot);
        return res.json(savedSpot);
      });
    } else {
      return res.status(304).send();
    }
  });
};

const deleteFreeDate = (req, res) => {
  ParkingSpot.findById(req.params.id, (err, spot) => {
    if (err)  return res.status(500);
    if (!spot) return res.status(404);
    let dateToDelete = moment(req.body.date).toDate();
    let indexOfObjectToRemove = indexOfSameDate(spot.freeDates, dateToDelete);
    if (indexOfObjectToRemove !== -1) {
      spot.freeDates.splice(indexOfObjectToRemove, 1);
      spot.save((err, savedSpot) => {
        if (err)  return res.status(500);
        console.log(savedSpot);
        return res.json(savedSpot);
      });
    } else {
      return res.status(304).send(spot);
    }
  });
};

const indexOfSameDate = (dates, dateToFind) => {
  let foundDate = dates.find(date => moment(date).isSame(dateToFind, "day"));
  if (foundDate) {
    return dates.indexOf(foundDate);
  }
  return -1;
};

const create = (req, res) => {
  ParkingSpot.create(req.body).exec((err, spot) => {
    if (err)  return console.log(err);
    return res.json(spot);
  });
};

const getBorrowEventsForSpot = (req, res) => {
  BorrowEvent.find({spot: req.params.id}, (err, events) => {
    if (err) return res.status(500);
    return res.json(events);
  });
};

const createBorrowEvent = (req, res) => {
  ParkingSpot.findById(req.params.id, (err, spot) => {
    if (err) return res.status(500);
    if (!spot) return res.status(404);
    let newEvent = new BorrowEvent();

    newEvent.startTime = moment(req.body.startTime).toDate();
    newEvent.endTime = moment(req.body.endTime).toDate();
    newEvent.borrower = req.user._id;
    newEvent.save((err, event) => {
      if (err) return res.status(500);
      spot.borrowEvents.push(event._id);
      spot.save((err, spot) => {
        if (err) return res.status(500);
        return res.json(spot);
      });
    });
  });
};

module.exports = {
  getById,
  getAll,
  create,
  addFreeDates,
  deleteFreeDate,
  getMySpots,
  getBorrowEventsForSpot,
  createBorrowEvent
};
