const User = require('../models/user');

const protected = (req, res) => res.json({success: true, user: req.user});

const getAll = (req, res) => User.find((err, users) => res.json(users));

const find = (req, res) => User.findOne({'_id': req.params.userId}, (err, user) => res.json(user));

module.exports = {protected, getAll, find};
