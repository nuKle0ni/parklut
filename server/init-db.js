const ParkingArea = require('./models/parking-area');
const ParkingSpot = require('./models/parking-spot');
const User = require('./models/user');
const lutParkingAreas = [{
  name: "P6",
  location: {lat: 61.064195, lng: 28.089608},
  parkingSpots: []
}, {
  name: "P5",
  location: {lat: 61.065674, lng: 28.089238},
  parkingSpots: []
}, {
  name: "P8",
  location: {lat: 61.067251, lng: 28.090383},
  parkingSpots: []
}, {
  name: "P4",
  location: {lat: 61.066432, lng: 28.090257},
  parkingSpots: []
}
];

const lutParkingSpots = {
  P6: [
    {name: "P6-10"},
    {name: "P6-11"},
    {name: "P6-12"},
    {name: "P6-13"},
    {name: "P6-14"},
  ],
  P5: [
    {name: "P5-30"},
    {name: "P5-31"},
    {name: "P5-32"},
    {name: "P5-33"},
    {name: "P5-34"},
  ],
  P8: [
    {name: "P8-20"},
    {name: "P8-21"},
    {name: "P8-22"},
    {name: "P8-23"},
    {name: "P8-24"},
  ],
  P4: [
    {name: "P4-40"},
    {name: "P4-41"},
    {name: "P4-42"},
    {name: "P4-43"},
    {name: "P4-44"},
  ]
};

const studentUser = new User();
studentUser.email = "student";
studentUser.password = "student";

let teacherUser = new User();
teacherUser.email = "teacher";
teacherUser.password = "teacher";

const adminUser = new User();
adminUser.email = "admin";
adminUser.password = "admin";
adminUser.admin = true;

const setTeacherAsTenant = (p6_20_spot) => {
  p6_20_spot.tenant = teacherUser._id;
  console.log(teacherUser._id);
  p6_20_spot.save((err, spot) => console.log("Set teacher as tenant"));
};

const createLutParkingAreas = () => {
  lutParkingAreas.forEach(area => ParkingArea.create(area, (err, area) => {
    if (err) console.log(err);
    lutParkingSpots[area['name']].forEach(spot => {
      ParkingSpot.create(spot, (err, savedSpot) => {
        if (savedSpot.name == "P6-10") {
          setTeacherAsTenant(savedSpot);
        }
        ParkingArea.update({"_id": area._id}, {"$push": {"parkingSpots": savedSpot._id}}, (err, res) => console.log("Created"));
      });
    });
  }));
};

function createTestUsers() {
  adminUser.save((err, user) => console.log("saved admin user"));
  studentUser.save((err, res) => console.log("saved student user"));
  teacherUser.save((err, user) => teacherUser = user);
}

const initIfNeeded = () => {
  ParkingArea.find((err, areas) => {
    if (areas.length == 0) {
      createLutParkingAreas();
      createTestUsers();
    }
  });
};

module.exports = initIfNeeded;
