const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BorrowEventSchema = new Schema({
  startTime: Date,
  endTime: Date,
  borrower: {type: Schema.Types.ObjectId, ref: "User"},
});

module.exports = mongoose.model('BorrowEvent', BorrowEventSchema);
