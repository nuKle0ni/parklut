const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const deepPopulate = require('mongoose-deep-populate')(mongoose);

const ParkingAreaSchema = new Schema({
  name: String,
  location: {lat: Number, lng: Number},
  parkingSpots: [{type: Schema.Types.ObjectId, ref: "ParkingSpot"}]
});

ParkingAreaSchema.plugin(deepPopulate);
module.exports = mongoose.model('ParkingArea', ParkingAreaSchema);
