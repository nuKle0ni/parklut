const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ParkingSpotSchema = new Schema({
  name: String,
  tenant: {type: Schema.Types.ObjectId, ref: 'User'},
  borrowEvents: [{type: Schema.Types.ObjectId, ref: 'BorrowEvent'}],
  location: {lat: Number, lon: Number},
  freeDates: [Date]
});

module.exports = mongoose.model('ParkingSpot', ParkingSpotSchema);
