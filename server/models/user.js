const mongoose = require('mongoose');
const bcrypt = require('mongoose-bcrypt');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: String,
  email: {type: String, required: [true, 'Email cannot be empty'], unique: [true, 'User already exists.']},
  password: {type: String, required: true, bcrypt: true},
  parkingSpots: [{type: Schema.Types.ObjectId, ref: 'ParkingSpot'}],
  admin: {type: Boolean, default: false}
});

UserSchema.plugin(bcrypt);
UserSchema.plugin(uniqueValidator);

module.exports = mongoose.model('User', UserSchema);
