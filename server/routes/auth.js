const express = require('express');
const authCtrl = require('../controllers/auth');
const passport = require('passport');

const router = express.Router();

router.post('/login', passport.authenticate('local', {session: false}), authCtrl.login);

router.post('/register', authCtrl.register);

module.exports = router;
