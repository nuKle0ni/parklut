const express = require('express');
const eventCtrl = require('../controllers/borrow-event');
const protectRoute = require('../protect-route');

const router = express.Router();

router.get('/', protectRoute, eventCtrl.getAll);

router.post('/', protectRoute, eventCtrl.create);

module.exports = router;
