const express = require('express');
const userRoutes = require('./user');
const authRoutes = require('./auth');
const parkingAreaRoutes = require('./parking-area');
const parkingSpotRoutes = require('./parking-spot');
const borrowEventRoutes = require('./borrow-event');

const router = express.Router();

router.get('/', (req, res) => res.send('OK'));

router.use('/users', userRoutes);

router.use('/auth', authRoutes);

router.use('/parking-areas', parkingAreaRoutes);

router.use('/parking-spots', parkingSpotRoutes);

router.use('/borrow-events', borrowEventRoutes);

module.exports = router;
