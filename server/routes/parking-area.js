const express = require('express');
const areaCtrl = require('../controllers/parking-area');
const protectRoute = require('../protect-route');

const router = express.Router();

router.get('/', protectRoute, areaCtrl.getAll);
router.get('/:id', protectRoute, areaCtrl.getById);

module.exports = router;
