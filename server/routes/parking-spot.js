const express = require('express');
const spotCtrl = require('../controllers/parking-spot');
const protectRoute = require('../protect-route');

const router = express.Router();

router.get('/my-spots', protectRoute, spotCtrl.getMySpots);

router.get('/:id/borrow-events', protectRoute, spotCtrl.getBorrowEventsForSpot);

router.get('/:id', protectRoute, spotCtrl.getById);

router.get('/', protectRoute, spotCtrl.getAll);

router.post('/', protectRoute, spotCtrl.create);

router.post('/:id/borrow-events', protectRoute, spotCtrl.createBorrowEvent);

router.patch('/:id/free-dates', protectRoute, spotCtrl.addFreeDates);

router.delete('/:id/free-dates', protectRoute, spotCtrl.deleteFreeDate);

module.exports = router;
