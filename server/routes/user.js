const express = require('express');
const passport = require('passport');
const userCtrl = require('../controllers/user');

const router = express.Router();

router.use(passport.authenticate('jwt', {session: false}));

router.get("/protected", userCtrl.protected);

router.get('/', userCtrl.getAll);

router.get('/:userId', userCtrl.find);

module.exports = router;
