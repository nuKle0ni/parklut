const config = require('./config.js');
const express = require('express');
const passport = require('passport');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const initDB = require('./init-db.js');
const cors = require('cors'); // allows cross origin requests.
const morgan = require('morgan');
const routes = require('./routes');
const emailPasswordStrategy = require('./auth-strategies.js').localStrategy;
const jwtStrategy = require('./auth-strategies.js').jwtStrategy;
const hostname = '127.0.0.1';
const port = process.env.PORT || 3000;

//connect to MongoDB using env variable IP (Cloud9) or just localhost. Uses default port 27017.
mongoose.connect(config.mongoUrl);

passport.use(emailPasswordStrategy);
passport.use(jwtStrategy);

const app = express();
app.use(morgan('combined'));
app.use(cors());
app.use(bodyParser.json());
app.use(express.static('../client/LUTPark/www'));
app.use(passport.initialize());
app.use('/api', routes);

initDB();

app.listen(port, () => console.log(`Express ready http://${hostname}:${port}`));
